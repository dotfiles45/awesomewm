require("main.utils")

-- Mouse
run_once({'xinput set-prop "ETPS/2 Elantech Touchpad" "libinput Tapping Enabled" 1'})
run_once({'xinput set-prop "ETPS/2 Elantech Touchpad" "libinput Middle Emulation Enabled" 1'})

-- Monitor
run_once({"xrandr --output eDP-1 --primary --output HDMI-1 --off --output DP-1 --off"})
run_once({"xrandr --output HDMI-1-0 --auto --right-of eDP-1"})

run_once({"megasync"})