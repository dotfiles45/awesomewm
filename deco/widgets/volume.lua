-- Wibox handling library
local wibox = require("wibox")

local utils = require("main.utils")

local function factory(args)
    local function getVolumePercentage()
        return os.capture('amixer | grep -o -P ".{0,2}%" | head -c 2')
    end

    local volume = {
        markup = getVolumePercentage(),
        align  = 'center',
        valign = 'center',
        widget = wibox.widget.textbox
    }

    return volume
end

return factory
