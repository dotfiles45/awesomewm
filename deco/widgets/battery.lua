-- Wibox handling library
local wibox = require("wibox")
local awful = require("awful")

local utils = require("main.utils")

local function factory(args)
    local command = "echo Battery: $(cat /sys/class/power_supply/BAT0/capacity)"

    local bat = {
        align  = 'center',
        valign = 'center',
        widget = wibox.widget.textbox
    }
    local bat, timmer = awful.widget.watch('bash -c "' .. command .. '"', 360)

    return bat
end

return factory
